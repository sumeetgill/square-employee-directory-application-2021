//
//  EmployeeListViewModel.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-27.
//

import Foundation


class EmployeeListViewModel: ObservableObject, Identifiable {
    
    @Published var dataSource: [EmployeeViewModel]? = nil
    private var employeeListService: EmployeeListService
    
    public var employeeCount: Int {
        return dataSource?.count ?? 0
    }
    
    init(employeeListService: EmployeeListService) {
        self.employeeListService = employeeListService
        refresh()
    }
    
    func refresh() {
        employeeListService.getEmployeeList(endpoint: EmployeeEndpoint.regular.rawValue) { result in
            switch result {
            case .success(let employeeList):
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    let sortedEmployees = employeeList.employees.sorted(by: { $0.full_name < $1.full_name })
                    let sortedEmployeeList = EmployeeList(employees: sortedEmployees)
                    self.dataSource = EmployeeListViewModelBuilder.viewModel(from: sortedEmployeeList)
                }
            case .failure(let error):
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.dataSource = nil
                }
                debugPrint("Error fetching employeeList: \(error)")
            }
        }
    }
}

struct EmployeeListViewModelBuilder {
    static func viewModel(from data: EmployeeList) -> [EmployeeViewModel] {
        var dataArray = [EmployeeViewModel]()
        for employee in data.employees {
            dataArray.append(EmployeeViewModel(from: employee))
        }
        return dataArray
    }
}
