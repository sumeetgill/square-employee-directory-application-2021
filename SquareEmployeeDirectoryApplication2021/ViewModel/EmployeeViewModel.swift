//
//  EmployeeViewModel.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-27.
//

import Combine
import UIKit.UIImage
import Foundation

class EmployeeViewModel {
    let uuid: String
    let fullName: String
    let phoneNumber: String?
    let emailAddress: String
    let biography: String?
    let photoSmallURL: URL?
    let photoLargeURL: URL?
    let team: String
    let employeeType: EmployeeType
    
    init(uuid: String,
         fullName: String,
         phoneNumber: String?,
         emailAddress: String,
         biography: String,
         photoSmallURL: URL,
         photoLargeURL: URL,
         team: String,
         employeeType: EmployeeType) {
        self.uuid = uuid
        self.fullName = fullName
        self.phoneNumber = phoneNumber
        self.emailAddress = emailAddress
        self.biography =  biography
        self.photoSmallURL = photoSmallURL
        self.photoLargeURL = photoLargeURL
        self.team = team
        self.employeeType = employeeType
    }
    
     init(from employee: Employee) {
        self.uuid = employee.uuid
        self.fullName = employee.full_name
        self.phoneNumber = employee.phone_number
        self.emailAddress = employee.email_address
        self.biography =  employee.biography
        self.photoSmallURL = employee.photo_url_small
        self.photoLargeURL = employee.photo_url_large
        self.team = employee.team
        self.employeeType = employee.employee_type
    }
    
    var initials: String {
        let components = self.fullName.components(separatedBy: " ").compactMap({ $0.first })
        return String(components).uppercased()
    }
}
