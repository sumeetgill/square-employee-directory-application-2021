//
//  ViewController.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-26.
//
import Combine
import UIKit

class ViewController: UIViewController {
    @IBAction func pushView(_ sender: Any) {
        let vc = EmployeeListTableViewController(style: .plain)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the views
    }
}

