//
//  Employee.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-26.
//

import Foundation

struct EmployeeList: Codable {
    let employees: [Employee]
}

struct Employee: Codable {
    let uuid: String
    let full_name: String
    let phone_number: String?
    let email_address: String
    let biography: String?
    let photo_url_small: URL?
    let photo_url_large: URL?
    let team: String
    let employee_type: EmployeeType
}

enum EmployeeType: String, Codable {
    case FULL_TIME
    case PART_TIME
    case CONTRACTOR
    case OTHER
    
    func title() -> String {
        switch self {
        case .FULL_TIME:
            return "Full Time"
        case .PART_TIME:
            return "Part Time"
        case .CONTRACTOR:
            return "Contractor"
        default:
            return "Other"
        }
    }
}
