//
//  NetworkService.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-26.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case head = "HEAD"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    case patch = "PATCH"
}

protocol NetworkServiceProtocol {
    @discardableResult
    func createDataTask<DataType>(path: String, httpMethod: HTTPMethod, completion: @escaping (Result<DataType>) -> Void) -> URLSessionDataTask where DataType: Decodable
    
    func createRequest(url: URL, httpMethod: HTTPMethod) -> URLRequest
    
    func mapResponse<DataType>(response: URLResponse?, data: Data?, error: Error?) -> Result<DataType> where DataType: Decodable
    
    
}

class NetworkService: NetworkServiceProtocol {
    let baseURL = URL(string: "https://s3.amazonaws.com/sq-mobile-interview/")!

    @discardableResult
    func createDataTask<DataType>(path: String, httpMethod: HTTPMethod, completion: @escaping (Result<DataType>) -> Void) -> URLSessionDataTask where DataType: Decodable {
        let url = baseURL.appendingPathComponent(path)
        let request = createRequest(url: url, httpMethod: httpMethod)
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { data, response, error in
            if let data = data {
                debugPrint("Response Endpoint: \(String(describing: response?.url))")
                debugPrint("Response Data: \(String(data: data, encoding: .utf8) ?? "Error getting response data"))")
            }
            
            let response:Result<DataType> = self.mapResponse(response: response, data: data, error: error)
            completion(response)
        }
        
        return task
    }
    
    internal func createRequest(url: URL, httpMethod: HTTPMethod) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        
        return request
    }

    internal func mapResponse<DataType>(response: URLResponse?, data: Data?, error: Error?) -> Result<DataType> where DataType: Decodable {
        guard let httpResponse = response as? HTTPURLResponse else {
            return .failure(NetworkServiceError.generic("HTTP Response invalid"))
        }
        
        switch httpResponse.statusCode {
        case 200..<300:
            if let data = data {
                let dataResult: Result<Data> = .success(data)
                do {
                    let decodedDataResult = try dataResult.decoded(as: DataType.self)
                    return .success(decodedDataResult)
                } catch let error {
                    if let decodingError = error as? DecodingError {
                        debugPrint("Decoding Error: \(decodingError)")
                    }
                    return .failure(NetworkServiceError.malformedJSON)
                }
            } else if let error = error {
                return .failure(error)
            } else {
                return .failure(ResultError.missingData)
            }
        case 500..<600:
            return .failure(NetworkServiceError.generic("HTTP Response: \(httpResponse.statusCode)"))
        default:
            return .failure(NetworkServiceError.generic("Unknown error while mapping response"))
        }
    }
}

