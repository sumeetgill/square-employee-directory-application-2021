//
//  NetworkService+Image.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-27.
//

import Foundation


import Foundation
import UIKit.UIImage
import Combine

protocol ImageLoadingService: AnyObject {
    func loadImage(from url: String, completion: @escaping (UIImage?) -> Void) -> URLSessionDataTask?
}

enum ImageSize {
    case small
    case large
}

extension NetworkService: ImageLoadingService {
    func loadImage(from url: String, completion: @escaping (UIImage?) -> Void) -> URLSessionDataTask? {
        let task = createDataTask(path: url,
                                  httpMethod: .get,
                                  completion: ({ (responseResult: Result<NetworkServiceSingleValueResponse<Data>>) in
                                    let result = self.mapSingleValueResponse(responseResult: responseResult)
                                    
                                    switch result {
                                    case .success(let data):
                                        let image = UIImage(data: data)
                                        completion(image)
                                    case .failure(let error):
                                        debugPrint("Error downloading image: \(error)")
                                        completion(nil)
                                    }
                                  }))
        task.resume()
        return task
    }
}

