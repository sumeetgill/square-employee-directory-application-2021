//
//  NetworkService+Response.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-26.
//

import Foundation

public enum Result<Value> {
    case success(Value)
    case failure(Error)
    
    func resolve() throws -> Value {
        switch self {
        case .success(let value):
            return value
        case .failure(let error):
            throw error
        }
    }
}

extension Result where Value == Data {
    func decoded<T: Decodable>(as: T.Type) throws -> T {
        let decoder = JSONDecoder()
        let data = try resolve()
        return try decoder.decode(T.self, from: data)
    }
}

enum ResultError: Error {
    case missingData
}


