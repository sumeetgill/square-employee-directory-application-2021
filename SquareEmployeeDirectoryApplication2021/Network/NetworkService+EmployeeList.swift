//
//  NetworkService+EmployeeList.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-26.
//

import Foundation

protocol EmployeeListService {
    @discardableResult
    func getEmployeeList(endpoint: String,
                         completion: @escaping (Result<EmployeeList>) -> Void) -> URLSessionDataTask?
    
}

enum EmployeeEndpoint: String {
    case regular = "employees.json"
    case malformed = "employees_malformed.json"
    case empty = "employees_empty.json"
}

extension NetworkService: EmployeeListService {

    @discardableResult
    func getEmployeeList(endpoint: String = EmployeeEndpoint.regular.rawValue,
                         completion: @escaping (Result<EmployeeList>) -> Void) -> URLSessionDataTask? {
        let task = createDataTask(path: endpoint,
                                  httpMethod: .get,
                                  completion: ({ (responseResult: Result<EmployeeList>) in
                                        completion(responseResult)
                                  }))
        task.resume()
        return task
    }
}

