//
//  NetworkService+Error.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-26.
//

import Foundation

extension NetworkService {
    enum NetworkServiceError: Error {
        case malformedJSON
        case emptyResponse
        case invalidURL
        case unknown
        case generic(String)
    }
}
