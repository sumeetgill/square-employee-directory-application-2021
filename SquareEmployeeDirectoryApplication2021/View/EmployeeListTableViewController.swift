//
//  EmployeeListTableViewController.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-29.
//

import UIKit
import Combine

class EmployeeListTableViewController: UITableViewController {
    private var cancellables = Set<AnyCancellable>()
    private var employeeListVMs: [EmployeeViewModel]? = nil {
        didSet {
            tableView.disableBackgroundText()
            if let value = employeeListVMs, value.count == 0 {
                tableView.setBackgroundText(text: "Employee directory is empty.")
            } else if employeeListVMs == nil {
                tableView.setBackgroundText(text: "Unable to load employee directory.")
            }
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Employee Directory"
        
        self.tableView.register(UINib(nibName: EmployeeListRowCell.identifier, bundle: nil), forCellReuseIdentifier: EmployeeListRowCell.identifier)
        
        let networkService = NetworkService()
        let viewmodel = EmployeeListViewModel(employeeListService: networkService)
        
        viewmodel
            .$dataSource
            .receive(on: RunLoop.main)
            .sink { (data) in
                self.employeeListVMs = data
                debugPrint("View Models: \(String(describing: self.employeeListVMs))")
            }.store(in: &cancellables)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = employeeListVMs {
            return data.count
        }
        
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeListRowCell.identifier)
                as? EmployeeListRowCell else {
            fatalError("Cannot dequeue cell")
        }
        if let data = employeeListVMs {
            cell.bind(viewModel: data[indexPath.row])
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension UITableView {
    func setBackgroundText(text: String) {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        label.text = text
        label.textAlignment = .center
        label.sizeToFit()
        
        self.backgroundView = label
        self.separatorStyle = .none
        self.isScrollEnabled = false
    }
    
    func disableBackgroundText() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
        self.isScrollEnabled = true
    }
}
