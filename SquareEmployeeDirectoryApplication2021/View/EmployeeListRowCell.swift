//
//  EmployeeListRowCell.swift
//  SquareEmployeeDirectoryApplication2021
//
//  Created by Sumeet Gill on 2021-06-29.
//

import UIKit
import Kingfisher

class EmployeeListRowCell: UITableViewCell {
    
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var profileImageLabel: UILabel!
    
    @IBOutlet var profileNameLabel: UILabel!
    @IBOutlet var profileTeamLabel: UILabel!
    @IBOutlet var profileEmployeeType: UILabel!
    
    @IBOutlet var verticalStackView: UIStackView!
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        postInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    private func postInit() {
        configureView()
    }
    
    private func configureView() {
        verticalStackView.alignment = .leading
        verticalStackView.axis = .vertical
        verticalStackView.spacing = 5
        verticalStackView.distribution = .equalSpacing
                
        profileImageLabel.font = .systemFont(ofSize: 20)
        profileImageLabel.textAlignment = .center

        profileNameLabel.font = .systemFont(ofSize: 20)
        profileNameLabel.textAlignment = .left
        
        profileTeamLabel.font = .systemFont(ofSize: 10)
        profileTeamLabel.textAlignment = .left
        
        profileEmployeeType.font = .systemFont(ofSize: 10)
        profileEmployeeType.textAlignment = .left
    }
        
    private func configureConstraints() {
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        profileImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        profileImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        profileImageView.heightAnchor.constraint(lessThanOrEqualTo: contentView.heightAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalTo: profileImageView.heightAnchor, multiplier: 1).isActive = true
        
        profileImageLabel.translatesAutoresizingMaskIntoConstraints = false
        profileImageLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        profileImageLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        profileImageLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        profileImageLabel.heightAnchor.constraint(lessThanOrEqualTo: contentView.heightAnchor).isActive = true
        profileImageLabel.widthAnchor.constraint(equalTo: profileImageLabel.heightAnchor, multiplier: 1).isActive = true
        
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.leadingAnchor.constraint(equalTo: profileImageLabel.trailingAnchor, constant: 10).isActive = true
        verticalStackView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        verticalStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
    }
    
    func bind(viewModel: EmployeeViewModel?) {
        guard let viewModel = viewModel else {
            debugPrint("Unable to bind, no data")
            return
        }
        
        profileImageView.kf.setImage(with: viewModel.photoSmallURL)
        { result in
            switch result {
            case .success(_):
                self.profileImageView.isHidden = false
                self.profileImageLabel.isHidden = true
                self.profileImageLabel.text = ""
                self.configureConstraints()
            case .failure(let error):
                debugPrint("Error loading image: \(error)")
                self.profileImageView.isHidden = true
                self.profileImageLabel.isHidden = false
                self.profileImageLabel.text = viewModel.initials
                self.configureConstraints()
            }
        }
        
        profileNameLabel.text = viewModel.fullName
        profileTeamLabel.text = viewModel.team
        profileEmployeeType.text = viewModel.employeeType.title()
    }
}
