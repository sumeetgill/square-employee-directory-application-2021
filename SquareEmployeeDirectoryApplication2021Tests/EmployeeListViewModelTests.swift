//
//  EmployeeListViewModelTests.swift
//  SquareEmployeeDirectoryApplication2021Tests
//
//  Created by Sumeet Gill on 2021-07-02.
//

import XCTest
@testable import SquareEmployeeDirectoryApplication2021

class EmployeeListViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testEmployeeListVMRegular() throws {
        let service: EmployeeListService = MockEmployeeListService()
        let employeeVM = EmployeeListViewModel(employeeListService: service)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            XCTAssertEqual(employeeVM.employeeCount, 3)
        }
    }
    
    func testEmployeeListVMEmpty() throws {
        let service: EmployeeListService = MockEmployeeListEmptyService()
        let employeeVM = EmployeeListViewModel(employeeListService: service)
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            XCTAssertNotNil(employeeVM.dataSource)
            XCTAssertEqual(employeeVM.employeeCount, 0)
        }
    }
    
    func testEmployeeListVMBuilderEmpty() {
        let employeeList: EmployeeList = EmployeeList(employees: [])
        let vm = EmployeeListViewModelBuilder.viewModel(from: employeeList)
        XCTAssertEqual(vm.count, 0)
    }
    
    func testEmployeeListBuilderNormal() {
        let service = MockEmployeeListService()
        
        service.getEmployeeList(endpoint: "") { result in
            let employeeList = try? result.resolve()
            XCTAssertEqual(employeeList?.employees.count, 3)
        }
    }
}
