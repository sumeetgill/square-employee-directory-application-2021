//
//  MockClasses.swift
//  SquareEmployeeDirectoryApplication2021Tests
//
//  Created by Sumeet Gill on 2021-07-02.
//

import Foundation
@testable import SquareEmployeeDirectoryApplication2021

class MockEmployeeListService: EmployeeListService {
    init() {}
    
    func getEmployeeList(endpoint: String, completion: @escaping (Result<EmployeeList>) -> Void) -> URLSessionDataTask? {
        let employeeBort = Employee(uuid: "0",
                                    full_name: "Bort Smith",
                                    phone_number: "555-555-5555",
                                    email_address: "bort@simpsons.com",
                                    biography: nil, photo_url_small: nil,
                                    photo_url_large: nil, team: "Test",
                                    employee_type: EmployeeType.FULL_TIME)
        
        let employeeLisa = Employee(uuid: "1",
                                    full_name: "Lisa Simpson",
                                    phone_number: "555-555-5556",
                                    email_address: "lisa.simpson@simpsons.com",
                                    biography: nil, photo_url_small: nil,
                                    photo_url_large: nil, team: "Test",
                                    employee_type: EmployeeType.CONTRACTOR)
        
        let employeeBart = Employee(uuid: "2",
                                    full_name: "Bart Simpson",
                                    phone_number: "555-555-5557",
                                    email_address: "bart.simpson@simpsons.com",
                                    biography: nil, photo_url_small: nil,
                                    photo_url_large: nil, team: "Test 2",
                                    employee_type: EmployeeType.PART_TIME)
        
        let employeeList = EmployeeList(employees: [employeeBort, employeeLisa, employeeBart])
        completion(Result.success(employeeList))
        
        return nil
    }
}

class MockEmployeeListEmptyService: EmployeeListService {
    init() {}
    
    func getEmployeeList(endpoint: String, completion: @escaping (Result<EmployeeList>) -> Void) -> URLSessionDataTask? {
        let employeeList = EmployeeList(employees: [])
        completion(Result.success(employeeList))
        
        return nil
    }
}
