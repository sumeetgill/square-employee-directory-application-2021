//
//  NetworkServiceTests.swift
//  SquareEmployeeDirectoryApplication2021Tests
//
//  Created by Sumeet Gill on 2021-07-02.
//

import XCTest
@testable import SquareEmployeeDirectoryApplication2021

class NetworkServiceTests: XCTestCase {
    
    struct EquitableError: Equatable, Error { let message: String }

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testDecodedSingularJSON() throws {
        guard let path: String = Bundle(for: type(of: self)).path(forResource: "SingularEmployee", ofType: "json"),
              let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            XCTFail("Cannot fetch JSON file")
            return
        }
        
        let result = Result<Data>.success(data)
        let employeeList = try? result.decoded(as: EmployeeList.self)
        XCTAssertEqual(employeeList?.employees.count, 1)
    }
    
    func testDecodedMultipleJSON() throws {
        guard let path: String = Bundle(for: type(of: self)).path(forResource: "NormalEmployeeList", ofType: "json"),
              let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            XCTFail("Cannot fetch JSON file")
            return
        }
        
        let result = Result<Data>.success(data)
        let employeeList = try? result.decoded(as: EmployeeList.self)
        XCTAssertEqual(employeeList?.employees.count, 11)
    }
    
    func testDecodedEmptySON() throws {
        guard let path: String = Bundle(for: type(of: self)).path(forResource: "EmptyEmployeeList", ofType: "json"),
              let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            XCTFail("Cannot fetch JSON file")
            return
        }
        
        let result = Result<Data>.success(data)
        let employeeList = try? result.decoded(as: EmployeeList.self)
        XCTAssertEqual(employeeList?.employees.count, 0)
    }
    
    func testDecodedMalformedJSON() throws {
        guard let path: String = Bundle(for: type(of: self)).path(forResource: "MalformedEmployeeList", ofType: "json"),
              let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            XCTFail("Cannot fetch JSON file")
            return
        }
        
        let result = Result<Data>.success(data)
        let employeeList = try? result.decoded(as: EmployeeList.self)
        XCTAssertEqual(employeeList?.employees.count, nil)
    }
}
