# Square Employee Directory
## Created by Sumeet Gill

# Dependencies
I imported Kingfisher in to handle my asynchronous image downloading and caching. It is imported via swift packages.

# Branches

I have two branches on this project. 

#### Main
The main branch will have the requirements set out for this challenge. I created the UI using UIKit aand used the MVVM design pattern for my code. 

#### Combine
The combine branch was a chance for me to experiment using SwiftUI and Combine in a project. I thought this was a good opportunity for me to learn SwiftUI and Combine better. 

I did not include unit testing in this version of the app, however, I believe every other requirement has been done. 

One big difference I noted was how little code is required to achieve the same reuslt compared to the main branch, especially for UI related tasks. 

# Design Notes

I have noticed that it may take a little bit for the start button to appear in simulator. I am not sure if this is the fault of my macbook or not. However, once it appears you will be able to click on it to view the tableview for employees. I chose to do this method as I figured it would be a feature in an existing app rather than a standalone feature, so I wanted to make the lifecycle flow the same.

# Target Deployment

 I tested the UI for iPhones.
 
 # Unit Testing
 
 I definitely wasn't able to get the coverage I would like, due to time constaints. A gap that I would test if I had extra time would be the mapping from network layer.
 
 # Hosting
 
 This project is hosted on bitbucket @ https://bitbucket.org/sumeetgill/square-employee-directory-application-2021/ , please request to view if you want to see it.


